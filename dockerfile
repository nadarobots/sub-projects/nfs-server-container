FROM ubuntu

ENV DEBIAN_FRONTEND noninteractive

ADD nfs.init /etc/sv/nfs/run
ADD nfs.stop /etc/sv/nfs/finish
ADD nfs_setup.sh /usr/local/bin/nfs_setup

RUN apt-get update -qq && \
    apt-get install -y netbase nfs-kernel-server runit inotify-tools -qq && \
    mkdir -p /exports $$ \
    mkdir -p /etc/sv/nfs

ADD nfs.init /etc/sv/nfs/run
ADD nfs.stop /etc/sv/nfs/finish
ADD nfs_setup.sh /usr/local/bin/nfs_setup

RUN chmod +x /etc/sv/nfs/run && \
    chmod +x /etc/sv/nfs/finish && \
    chmod +x /usr/local/bin/nfs_setup

RUN echo "nfs             2049/tcp" >> /etc/services
RUN echo "nfs             111/udp" >> /etc/services

VOLUME /exports

EXPOSE 111/udp 2049/tcp

ENTRYPOINT ["/usr/local/bin/nfs_setup"]
