# nfs-server-container

## Getting Started

Host
```
sudo apt update
sudo apt install nfs-kernel-server
sudo modprobe nfs
```

Run NFS Server
```
docker volume create nfs
docker run -d --name nfs -v nfs:/exports --privileged --net=host docker.io/nadarobots/nfs-server:latest /exports
```





alpine container to test with
```
docker run -d --name nfs --privileged --net=host -v /share/nfsshare:/nfsshare -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:latest
```